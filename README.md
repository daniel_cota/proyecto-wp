# Proyecto reto:  Desarrollar un manejador de proyectos
## Equipo: ***Who Is Wong?*** :floppy_disk:

## Manejador de Proyectos

Una empresa de desarrollo de software quiere llevar de mejor manera el control de sus proyectos,
por  eso  ha  solicitado  cotizaciones  a  diferentes  empresas  consultoras  externas  para  desarrollar
una plataforma acorde a sus necesidades.

## Requisitos

- La  empresa  requiere  llevar  los  expedientes  por  proyecto  de  desarrollo  con  la  siguiente
documentación  y  datos:  nombre  del  proyecto,  fecha  de  solicitud  de  proyecto,  fecha  de
arranque  del  proyecto,    descripción  del  proyecto,  proyect  manager  (scrum  master),  product
owner y su equipo de desarrollo involucrado.

- Por  cada  miembro  del  equipo  de  desarrollo  se  requiere  saber:  nombre  completo,  fecha
nacimiento, CURP, RFC, domicilio y una lista de habilidades.

- Las  habilidades  son  ranqueadas  por  junior,  senior  y  master.  Por  ejemplo  Juan  es  un
desarrollador java nivel master pero javascript nivel junior.

- Todos los usuario del sistema deben poder iniciar sesión en el sistema por medio de mínimo
tres redes sociales.

- Cada usuario cumple un rol dentro de un proyecto con diferentes permisos.

Cada  proyecto  esta  basado  por  estándar  en  la  metodología  de  desarrollo  de  software  ágil
SCRUM, el cual debe contar con el siguiente tablero de control:

- Debe  estar  divido  en  las  columnas  product  backlog,  release  backlog  (varias  por  tableros),  
sprint backlog (varios por backlog).

- Se debe poder cargar tarjetas o historias de usuario, estas historias deben tener los siguientes
datos :  

   - Cada tarjeta debe de poder dársele un valor de acuerdo a la secuencia fibonacci (0, 1, 1, 2, 3,
5, 8, 13).

   - Cada tarjeta debe actualizarse los tableros en tiempo real y debe de poder moverse las tarjetas
entre las columnas.

   - Debe permitir el calculo de la velocidad de desarrollo.

   - Debe crear un burndown chart por sprint ó release.

   - Debe permitir la estimación de tiempos de proyecto.

   - El scrum master debe poder monitorear el proyecto y revisar los avances de las tarjetas.

   - Debe permitir que el product owner valide o rechace las tarjetas.

   - Debe permitir realizar retrospectivas por cada release.

   - Debe generar un cierre de proyecto.

## Diagrama de Clases

![DiagramaProyecto](DiagramaProyecto.png)

### Software Necesario

Los software necesarios son los mencionados en el apartado de Requisitos Previos


- :arrow_right: [Git](https://git-scm.com/)
- :arrow_right: [Docker](https://www.docker.com/)
- :arrow_right: [Linux (Ubuntu)](https://ubuntu.com/)
- :arrow_right: [Node JS](https://nodejs.org/es/)
- :arrow_right: [Express](https://expressjs.com/es/)

Aparte de lo anterior se necesitará tener una cuenta en estas dos plataformas

- :arrow_right: [GitLab](https://gitlab.com/)
- :arrow_right: [DockerHub](https://hub.docker.com/)

## Proyectos

- [GitLab](https://gitlab.com/daniel_cota/proyecto-wp)
- [DockerHub](https://hub.docker.com/repository/docker/axeldali/projecto-wp)
- [Heroku](https://proyecto-web-platforms.herokuapp.com/)



## Autores

- Daniel Alberto Cota Ochoa     ***329701***
    - [GitLab](https://gitlab.com/daniel_cota)
- Axel Dalí Gomez Morales       ***329881***
    - [GitLab](https://gitlab.com/axel_dali)
- Maritrini Velázquez Ruiz     ***329675***
    - [GitLab](https://gitlab.com/Maritrini00)
