const express =require('express');

function list(req, res, next) {
    res.send('Rutas de backlogs');
}

function index(req,res,next){
    res.send(`Backlogs con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const featureList = req.body.featureList;//implicitos o sobre el cuerpo
    
    res.send(`Crear un backlog nuevo con FeatureList ${featureList} `);
}

function replace(req,res,next){
    res.send(`Remplazo un backlog con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del backlog con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un backlog con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}