const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Developers');
}

function index(req,res,next){
    res.send(`Developer con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const developerId = req.body.developerId;//implicitos o sobre el cuerpo
    res.send(`Crea un developer nuevo = ${developerId}`);
}

function replace(req,res,next){
    res.send(`Remplazo un developer con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Editar al developer con el ID = ${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`Eliminar al developer con el ID = ${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}