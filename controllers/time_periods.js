const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Periodo de tiempos');
}

function index(req,res,next){
    res.send(`periodo de tiempo con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const startDate = req.body.startDate;//implicitos o sobre el cuerpo
    const endDate = req.body.endDate;
    
    res.send(`Crear un periodo nuevo con inicio en: ${startDate} y final: ${endDate}`);
}

function replace(req,res,next){
    res.send(`Remplazo un periodo con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del periodo con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un periodo con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}