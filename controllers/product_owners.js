const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Product Owner');
}

function index(req,res,next){
    res.send(`Product Owner con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const productOwnerId = req.body.developerId;//implicitos o sobre el cuerpo
    res.send(`Crea un Product Owner nuevo = ${productOwnerId}`);
}

function replace(req,res,next){
    res.send(`Remplazo un Product Owner con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Editar al Product Owner con el ID = ${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`Eliminar al Product Owner con el ID = ${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}