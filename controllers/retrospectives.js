const express =require('express');

function list(req, res, next) {
    res.send('Rutas de retrospectivas');
}

function index(req,res,next){
    res.send(`retrospectivas con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const performance = req.body.performance;//implicitos o sobre el cuerpo
    const couldBeBetter = req.body.couldBeBetter;
    const commitment = req.body.commitment
    const email = req.body.email;
    
    res.send(`Crear una retroalimentación nueva con desempeño: ${performance} areas de oportunidad: ${couldBeBetter} y compromisos ${commitment}`);
}

function replace(req,res,next){
    res.send(`Remplazo una retroalimentación con ID =${req.params.id} por otra.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Remplazo propiedades de retroalimentación con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino una retroalimentación con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}