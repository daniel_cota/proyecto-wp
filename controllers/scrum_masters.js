const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Scrum Master');
}

function index(req,res,next){
    res.send(`Scrum Master con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const scrumMasterId = req.body.developerId;//implicitos o sobre el cuerpo
    res.send(`Crea un Scrum Master nuevo = ${scrumMasterId}`);
}

function replace(req,res,next){
    res.send(`Remplazo un Scrum Master con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Editar al Scrum Master con el ID = ${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`Eliminar al Scrum Master con el ID = ${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}