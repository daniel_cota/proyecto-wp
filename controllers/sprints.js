const express = require('express');

function list(req, res, next) {
    res.send('Rutas de sprints');
}

function index(req,res,next){
    res.send(`Sprint con el ID = ${req.params.id}`);
}

function create(req,res,next){
    const sprintFeatures = req.body.releaseList;
    const date = req.body.date;
    res.send(`Crear un sprint nuevo`);
}

function replace(req,res,next){
    res.send(`Remplazo un sprint con ID =${req.params.id} por otro.`);
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del sprint con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un sprint con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}
