const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Features');
}

function index(req,res,next){
    res.send(`Features con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const hourRequired = req.body.hourRequired;//implicitos o sobre el cuerpo
    const percentageRemaining = req.body.percentageRemaining;
    const startDate = req.body.startDate;
    
    res.send(`Crear un feature nuevo con ${hourRequired} horas requeridas , porcentaje: ${percentageRemaining} e inicio: ${startDate}`);
}

function replace(req,res,next){
    res.send(`Remplazo de un feature con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del feature con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un feature con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}