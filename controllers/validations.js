const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Validación');
}

function index(req,res,next){
    res.send(`Validación con un ID = ${req.params.id}`);
}

function create(req,res,next){
    const validate = req.body.developerId;//implicitos o sobre el cuerpo
    res.send(`Crea una Validación nueva = ${req.params.id}`);
}

function replace(req,res,next){
    res.send(`Remplazo una Validación con ID =${req.params.id} por otro.`);//params por el heather
}

function edit(req,res,next){
    res.send(`Editar la Validación con el ID = ${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`Eliminar la Validación con el ID = ${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}