const express = require('express');

function list(req, res, next) {
    res.send('Rutas de Releases');
}

function index(req,res,next){
    res.send(`Release con el ID = ${req.params.id}`);
}

function create(req,res,next){
    const sprintList = req.body.releaseList;
    res.send('Crear un release nuevo');
}

function replace(req,res,next){
    res.send(`Remplazo un release con ID =${req.params.id} por otro.`);
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del release con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un release con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}
