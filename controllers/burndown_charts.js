const express =require('express');

function list(req, res, next) {
    res.send('Rutas de Burndown Charts');
}

function index(req,res,next){
    res.send(`Burndown chart con el ID = ${req.params.id}`);
}

function create(req,res,next){
    const speed = req.body.speed;
    const points = req.body.points;
    res.send(`Crear un burndown chart nuevo con ${points} puntos y velocidad: ${speed}`);
}

function replace(req,res,next){
    res.send(`Remplazo un burndown chart con ID =${req.params.id} por otro.`);
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del burndown chart con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un burndown chart con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}
