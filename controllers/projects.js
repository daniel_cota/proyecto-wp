const express = require('express');

function list(req, res, next) {
    res.send('Rutas de Proyectos');
}

function index(req,res,next){
    res.send(`Proyecto con el ID = ${req.params.id}`);
}

function create(req,res,next){
    const project_name = req.body.project_name;
    const backlog = req.body.backlog;
    const releaseList = req.body.releaseList;
    res.send(`Crear un proyecto nuevo ${project_name}`);
}

function replace(req,res,next){
    res.send(`Remplazo un proyecto con ID =${req.params.id} por otro.`);
}

function edit(req,res,next){
    res.send(`Remplazo propiedades del proyecto con ID =${req.params.id} por otras.`);
}

function destroy(req,res,next){
    res.send(`elimino un proyecto con ID =${req.params.id} .`);
}

module.exports ={
    list, index, create, edit, replace, destroy
}
