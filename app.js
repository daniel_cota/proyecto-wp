var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var developersRouter = require('./routes/developers');
var scrumMastersRouter = require('./routes/scrum_masters');
var productOwnersRouter = require('./routes/product_owners');
var validationsRouter = require('./routes/validations');
var burndownRouter = require('./routes/burndown_charts');
var sprintsRouter = require('./routes/sprints');
var projectsRouter = require('./routes/projects');
var releasesRouter = require('./routes/releases');
var featuresRouter = require('./routes/features.js');
var retrospectivesRouter = require('./routes/retrospectives');
var timePeriodsRouter = require('./routes/time_periods');
var backlogsRouter = require('./routes/backlogs');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/developers', developersRouter);
app.use('/scrum_masters', scrumMastersRouter);
app.use('/product_owners', productOwnersRouter);
app.use('/validations', validationsRouter);
app.use('/burndown_charts', burndownRouter);
app.use('/sprints', sprintsRouter);
app.use('/projects', projectsRouter);
app.use('/releases', releasesRouter);
app.use('/features', featuresRouter);
app.use('/retrospectives', retrospectivesRouter);
app.use('/time_periods', timePeriodsRouter);
app.use('/backlogs', backlogsRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
